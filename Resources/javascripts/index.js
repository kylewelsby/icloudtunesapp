/**
* @depend app.js
**/

/*jslint white: false, browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true */
/*global $, Titanium, App */

$(document).ready(function() {
    // $("body").fadeIn();
    App.init();
    setTimeout(function(){
        $("#splashscreen").fadeOut('slow',function(){
            Titanium.UI.getCurrentWindow().hide();
        });
    },5000);
    // $("body").delay(1000).fadeOut();
});