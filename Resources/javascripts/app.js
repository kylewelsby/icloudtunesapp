/**
* @depend jquery/jquery-1.5.2.js
* @depend jquery/jquery-ui-1.8.9.custom.min.js
* @depend config.js
* @depend tracks.js
* @depend preferences.js
* @depend push.js
* @depend s3.js
**/
/*jslint browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true, white: false*/
/*global $, Config, Tracks, Preferences, Push, S3, Titanium, localStorage, sessionStorage */
/* vim: set ft=javascript: */

var App = (function() {
    return {
        init: function() {
            Titanium.API.set("App", App);
            App.initDatabase();
            App.initFileCache();
            Preferences.defaults();
            // if (Preferences.getBool("showInspector")) {
            //     Titanium.UI.getCurrentWindow().showInspector();
            //     Titanium.UI.UserWindow.showInspector(true);
            //     
            // }
            App.UI.createTray();

            if (App.Auth.signedIn()) {
                Titanium.API.log('info', 'User: Logged In' + Titanium.App.Properties.getString('userAuthToken'));
                var onlineCheck = setInterval(function() {
                    if (App.connected) {
                        clearInterval(onlineCheck);
                        App.Store.Online.get('tracks');
                    }
                },
                5000);
                if (Preferences.getBool("openPushOnStartup") === true) {
                    App.UI.showPushWindow();
                }
                if (!Titanium.Network.online) {
                    alert("Sorry, the weatherman says the clouds have gone away. \nPlease connect to the internet to bring them back.");
                }
            } else {
                App.Auth.logout();
            }
            $.ajaxSetup({
                error: function(event) {
                    if (event.status === 401) {
                        App.Auth.logout();
                    }
                }
            });
        },
        initDatabase: function() {
            var filepath = Titanium.Filesystem.getFile(Titanium.Filesystem.getApplicationDataDirectory(), 'iCloudTunes.db');
            App.database = Titanium.Database.openFile(filepath);
            // Something to reset database
            Tracks.init();
            // this.database.execute("CREATE TABLE IF NOT EXISTS `tracks` (`id` INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, `databaseID` INTEGER DEFAULT NULL, `name` VARCHAR(255) DEFAULT NULL, `artist` VARCHAR(255) DEFAULT NULL, `album` VARCHAR(255) DEFAULT NULL");
        },
        initFileCache: function(){
            App.Cache = {};
            var filepath = Titanium.Filesystem.getFile(Titanium.Filesystem.getApplicationDataDirectory(),'artwork');
            filepath.createDirectory();
            App.Cache.path = filepath.nativePath();
        },
        // connected: function() {
        //     return Titanium.Network.online;
        // },
        // init: function() {
        //     Titanium.API.set("App", App);
        //     Preferences.defaults();
        //
        //     if (Preferences.getBool("showInspector")) {
        //         Titanium.UI.getCurrentWindow().showInspector();
        //     }
        //     App.UI.createTray();
        //
        //     if (App.Auth.signedIn()) {
        //         Titanium.API.log('info', 'User: Logged In' + Titanium.App.Properties.getString('userAuthToken'));
        //         var onlineCheck = setInterval(function() {
        //             if (App.connected) {
        //                 clearInterval(onlineCheck);
        //                 App.Store.Online.get('tracks');
        //             }
        //         },
        //         5000);
        //         if (Preferences.getBool("openPushOnStartup") === true) {
        //             App.UI.showPushWindow();
        //         }
        //         if (!App.connected) {
        //             alert("Sorry, the weatherman says the clouds have gone away. \nPlease connect to the internet to bring them back.");
        //         }
        //     } else {
        //         App.Auth.logout();
        //     }
        //     $.ajaxSetup({
        //       error:function(event){
        //         if(event.status === 401){
        //           App.Auth.logout();
        //         }
        //       }
        //     });
        // },
        Auth: {
            signedIn: function() {
                if (Preferences.getString("userAuthToken").length > 0) {
                    // var xhr = Titanium.Network.createHTTPClient();
                    // xhr.open('POST', Config.host + '/users/show.json');
                    // xhr.send({
                    //   "api_key":Config.key,
                    //   "auth_token":Titanium.App.Properties.getString('auth_token')
                    // });
                    // if(xhr.readyState == 4){
                    //   return true;
                    // }else{
                    //   return false;
                    // }
                    return true;
                } else {
                    return false;
                }
            },
            logout: function() {
                Preferences.clear('userAuthToken', '');
                Titanium.API.log('info', 'User: Auth needed');
                App.UI.showLoginWindow();
                App.UI.updateTray();
            },
            token: function() {
                if (App.Auth.signedIn()) {
                    return Preferences.getString("userAuthToken");
                } else {
                    return false;
                }
            }
        },
        Store: {
            get: function(key) {
                return JSON.parse(localStorage.getItem(key));
            },
            set: function(key, value) {
                localStorage.setItem(key, JSON.stringify(value));
            },
            Session: {
                get: function(key) {
                    return JSON.parse(sessionStorage.getItem(key));
                },
                set: function(key, value) {
                    sessionStorage.setItem(key, JSON.stringify(value));
                },
                clear: function(key) {
                    sessionStorage.removeItem(key);
                }
            },
            Online: {
                set: function(key, value, callback) {
                    switch (key) {
                    case "tracks":
                        $.ajax({
                            url:
                            Config.host + "/api/v1/tracks.json",
                            dataType: "json",
                            type: "POST",
                            data: $.param(value) + "&auth_token=" + App.Auth.token() + "&api_key=" + Config.key,
                            beforeSend: function() {
                                App.Store.Session.clear('pushToAccount');
                            },
                            success: function(data) {
                                var artwork = Titanium.Filesystem.getFile(value.artwork),
                                xhr;
                                Push.loaded = data;
                                if (data.library.track.status === "pending") {
                                    App.Store.Session.set('pushToAccount', "pushing");
                                    // if(Config.env !== "development"){
                                    S3.Storage.put(
                                    data.library.track.cloud,
                                    data.library.track.filetype,
                                    Titanium.Filesystem.getFile(value.location)
                                    );
                                    // }
                                }
                                if (artwork.isFile()) {
                                    try {
                                        xhr = Titanium.Network.createHTTPClient();
                                        xhr.open("POST", Config.host + "/api/v1/artwork.json", true);
                                        xhr.send({
                                            data: artwork,
                                            track_id: data.library.track.id,
                                            api_key: Config.key
                                        });
                                    } catch(e) {
                                        console.error("Failed to send artwork");
                                        console.log(e);
                                    }
                                }
                                /*
                                var pushed = [];
                                if (App.Store.get('pushed')) {
                                    pushed = App.Store.get('pushed');
                                }
                                pushed.push(value.databaseID);
                                App.Store.set('pushed', pushed);
                                */
                            },
                            error: function() {
                                Push.loaded = null;
                                App.Store.Session.set('pushToAccount', "error");
                            }
                        });
                        break;
                    case "complete":
                        $.ajax({
                            url:
                            Config.host + "/api/v1/tracks/" + value + "/complete.json",
                            dataType: "json",
                            type: "POST",
                            data: "auth_token=" + App.Auth.token() + "&api_key=" + Config.key,
                            success: function(data) {
                                App.Store.Session.set('pushToAccount', "complete");
                                console.log("Successfully pushed to account");
                                Push.loaded = null;
                                Push.lock = false;
                            },
                            error: function() {
                                var pushed = [];
                                App.Store.Session.set('pushToAccount', "error");
                                console.error("Failed to push to account.");
                                App.Store.get('pushed');
                                pushed.pop(Push.loaded.library.database_id);
                                App.Store.set('pushed', pushed);

                                Push.loaded = null;
                                Push.lock = false;
                            }
                        });
                        break;
                    case "failed":
                        $.ajax({
                            url:
                            Config.host + "/api/v1/tracks/" + value + "/failed.json",
                            dataType: "json",
                            type: "POST",
                            data: $.param(value) + "auth_token=" + App.Auth.token() + "&api_key=" + Config.key,
                            success: function(data) {
                                App.Store.Session.set('pushToAccount', "failed");

                                Push.loaded = null;
                                Push.lock = false;
                                callback();
                            },
                            error: function() {
                                var pushed = [];
                                App.Store.Session.set('pushToAccount', "error");
                                console.error("Failed to push to account.");
                                App.Store.get('pushed');
                                pushed.pop(Push.loaded.library.database_id);
                                App.Store.set('pushed', pushed);

                                Push.loaded = null;
                                Push.lock = false;
                            }
                        });
                        break;
                    case "preferences":
                        $.ajax({
                            url:
                            Config.host + "/api/v1/preferences.json",
                            dataType: "json",
                            type: "PUT",
                            data: $.param(value) + "&auth_token=" + App.Auth.token() + "&api_key=" + Config.key,
                            success: function(data) {
                                Preferences.UI.onSync("success");
                            },
                            error: function() {
                                Preferences.UI.onSync("error");
                            }
                        });
                        break;
                    }
                },
                get: function(key) {
                    switch (key) {
                    case "tracks":
                        $.ajax({
                            url:
                            Config.host + "/api/v1/tracks.json",
                            dataType: "json",
                            type: "GET",
                            data: "auth_token=" + App.Auth.token() + "&api_key=" + Config.key,
                            success: function(data) {
                                var pushed = [];
                                $.each(data,
                                function() {
                                    if (this.database_id) {
                                        pushed.push(this.database_id);
                                    }
                                });
                                App.Store.set('pushed', pushed);
                            }
                        });
                        break;
                    case "preferences":
                        $.ajax({
                            url:
                            Config.host + "/api/v1/preferences.json",
                            dataType: "json",
                            type: "GET",
                            data: "auth_token=" + App.Auth.token() + "&api_key=" + Config.key,
                            success: function(data) {
                                Preferences.Online.results(data);
                                Preferences.UI.onSync("success");
                            },
                            error: function() {
                                Preferences.UI.onSync("error");
                            }
                        });
                        break;
                    }
                }
            },
            liveCheck: function(track) {
                $.ajax({
                    url: Config.host + "/api/v1/tracks/check",
                    dataType: "json",
                    type: "GET",
                    data: "track=" + track.name + "&artist=" + track.artist + "&album=" + track.album + "&auth_token=" + App.Auth.token,
                    success: function() {
                        return true;
                    },
                    error: function() {
                        return false;
                    }
                });
            },
            check: function(track) {
                var tracks = [];

                function inTracks(tracks, databaseID) {
                    var c = 0;
                    $.each(tracks,
                    function() {
                        if (this.databaseID === databaseID) {
                            c = c + 1;
                        }
                    });
                    if (c > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }

                if (App.Store.get('tracks')) {
                    tracks = App.Store.get('tracks');
                }

                if (tracks.length === 0 || !inTracks(tracks, track.databaseID)) {
                    if (!App.Store.liveCheck(track)) {
                        if (!inTracks(tracks, track.databaseID)) {
                            tracks.push(track);
                            App.Store.set('tracks', tracks);
                        } else {
                            App.Store.set('tracks', [track]);
                        }
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        },
        Animation: {
            replaceText: function(el, text, resize) {
                if ($.trim(text) !== $.trim(el.text())) {
                    if (el[0].nodeName === "INPUT") {
                        el.val(text);
                    } else {
                        el.wrapInner($("<span>", {
                            'class': 'replaceText'
                        }));
                        var elw = el.find('span.replaceText');
                        if (resize === true) {
                            el.animate({
                                width: elw.outerWidth(),
                                height: elw.height()
                            });
                        }
                        elw.fadeOut(150,
                        function() {
                            $(this).text(text);
                            if (resize === true) {
                                el.animate({
                                    width: elw.outerWidth(),
                                    height: elw.height()
                                },
                                150, 'swing',
                                function() {
                                    elw.fadeIn(250);
                                });
                            } else {
                                elw.fadeIn(250);
                            }
                        });
                    }
                }
            }
        },
        UI: {
            tray: null,
            close: null,
            trayItem: {
                pushStart: Titanium.UI.createMenuItem("Enable iCloudTunes",
                function() {
                    App.UI.showPushWindow();
                }),
                pushStop: Titanium.UI.createMenuItem("Disable iCloudTunes",
                function() {
                    $.each(Titanium.UI.getOpenWindows(),
                    function() {
                        if (this.getTitle().indexOf("Push") === -1) {
                            this.close();
                        }
                    });
                }),
                forcePush: Titanium.UI.createMenuItem("Push now!",
                function() {
                    Titanium.API.set("pushNow", true);
                }),
                logout: Titanium.UI.createMenuItem("Logout",
                function() {
                    Preferences.clear('userAuthToken', '');
                    App.UI.showLoginWindow();
                }),
                login: Titanium.UI.createMenuItem("Login",
                function() {
                    App.UI.showLoginWindow();
                }),
                preferences: Titanium.UI.createMenuItem("Preferences...",
                function() {
                    App.UI.showPreferencesWindow();
                }),
                quit: Titanium.UI.createMenuItem("Quit",
                function() {
                    Titanium.App.exit();
                })
            },
            closeWindows: function(title) {
                $.each(Titanium.UI.getOpenWindows(),
                function() {
                    if (this.getTitle().indexOf(title) > 1) {
                        // alert("Window already open.");
                        this.close();
                    }
                });
            },
            createTray: function() {
                App.UI.tray = Titanium.UI.addTray("app://icons/MenuBar.png");
                App.UI.updateTray();
            },
            updateTray: function() {
                var trayMenu = Titanium.UI.createMenu();

                App.UI.tray.setHint("iCloudTunes - Re-discover your music anywhere.");
                App.UI.tray.setMenu(trayMenu);
                Titanium.API.set("trayItems", App.UI.trayItem);
                setInterval(function() {
                    if (Titanium.App.Properties.getBool('locked') === true) {
                        App.UI.trayItem.forcePush.disable();
                    } else {
                        App.UI.trayItem.forcePush.enable();
                    }
                },
                1000);

                if (App.Auth.signedIn()) {
                    trayMenu.appendItem(App.UI.trayItem.pushStart);
                    trayMenu.appendItem(App.UI.trayItem.forcePush);
                    trayMenu.addSeparatorItem();
                    trayMenu.appendItem(App.UI.trayItem.logout);
                } else {
                    trayMenu.appendItem(App.UI.trayItem.login);
                }
                trayMenu.addSeparatorItem();
                trayMenu.appendItem(App.UI.trayItem.preferences);
                // trayMenu.appendItem(help);
                trayMenu.addSeparatorItem();
                trayMenu.appendItem(App.UI.trayItem.quit);
            },
            showLoginWindow: function() {
                App.UI.updateTray();
                App.UI.closeWindows("Push");
                App.UI.closeWindows("Login");
                Titanium.UI.getMainWindow().hide();
                Titanium.UI.createWindow("app://login.html").open();
            },
            showPreferencesWindow: function() {
                $.each(Titanium.UI.getOpenWindows(),
                function() {
                    if (this.getTitle() === "iCloudTunes - Preferences") {
                        this.close();
                    }
                });
                Titanium.UI.getMainWindow().hide();
                Titanium.UI.createWindow('app://preferences.html').open();
            },
            showPushWindow: function() {
                App.UI.trayItem.pushStart.setLabel("Disable iCloudTunes");
                if (App.UI.close === null) {
                    Titanium.UI.getMainWindow().hide();
                    Titanium.UI.createWindow('app://push.html').open();
                    App.UI.close = "Push";
                } else {
                    App.UI.closeWindows("Push");
                    App.UI.trayItem.pushStart.setLabel("Enable iCloudTunes");
                    App.UI.close = null;
                }
            },
            reset: {
                forcePush: function() {
                    var i = Titanium.API.get("App").UI.trayItem.forcePush;
                    i.label = "Push now!";
                    i.addEventListener('click',
                    function(e) {
                        if (Titanium.API.get("Push")) {
                            var track = Titanium.API.get("Push").track;
                            Titanium.API.get("Push").startWorker(track);
                        }
                    });
                    i.enable();
                },
                enablePush: function() {

                    }
            }
        }
    };
} ());
