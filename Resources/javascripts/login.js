/**
* @depend app.js
**/
/*jslint white: false, browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true */
/*global window, $, Titanium, App, Config */

var Login = (function() {
    return {
        init: function() {
            $("a.button#goToCreateAccount").bind("click",
            function(e) {
                e.preventDefault();
                var toggle, text, el = $("a.button#goToCreateAccount");
                if($("#innerWrapper").css('margin-left') === '0px'){
                    toggle = "-400px";
                    App.Animation.replaceText(el, "Login", false);
                    text = "Login";
                }else{
                    toggle = "0px";
                    App.Animation.replaceText(el, "Create new account", false);
                    // text = "Create new account";
                }
                App.Animation.replaceText($("#new_session legend"), "Welcome to iCloudTunes",false);
                App.Animation.replaceText($("#new_account legend"), "Create an account",false);
                $("div.errorMessage:visible").slideUp('slow');
                $("#innerWrapper").animate({
                    marginLeft: toggle
                },
                1000, 'easeInOutExpo',function(){
                    // el.text(text);
                });
            });
            
            $.ajaxSetup({
                beforeSend: function(xhr) {
                    App.Animation.replaceText($("input[type=submit]"), "Cancel", false);
                    App.Animation.replaceText($("#new_session legend"), "...Logging in",false);
                    App.Animation.replaceText($("#new_account legend"), "...Creating account",false);
                    $("div.errorMessage:visible").slideUp('slow');
                    xhr.setRequestHeader("api_key", Config.key);
                },
                complete: function(xhr) {
                    window.ajax_call = undefined;
                    App.Animation.replaceText($("#new_session :input[type=submit]"), "Sign In", false);
                    App.Animation.replaceText($("#new_account :input[type=submit]"), "Create Account", false);
                },
                success: function(data){
                    Titanium.App.Properties.setString('userAuthToken', data.user.authentication_token);
                    $("body").removeClass('error').addClass("success");
                    App.Animation.replaceText($("#new_session legend"), "Yay, Logged in.", false);
                    App.Animation.replaceText($("#new_account legend"), "Wow, Account created.", false);
            
                    setTimeout(function() {          
                        Titanium.UI.getCurrentWindow().close();
                        Titanium.App.restart();
                    },
                    2000);
                },
                error: function(xhr){
                    var errorMessage;
                    try{
                        errorMessage = JSON.parse(xhr.responseText).error;
                    }catch(e){
                        errorMessage = "Your credentials where incorrect. Please try again."; 
                    }
                    switch (xhr.status) {
                    case 404:
                        errorMessage = "Please try updating your version of iCloudTunes to the latest version.";
                        break;
                    case 503:
                        errorMessage = "The cloud is being re-formed, nice fluffy clouds will be here soon.";
                        break;
                    case 500:
                        errorMessage = "It appears we're in the middle of the desert, someone will be rescuing you soon. ";
                        break;

                    case 50[1-9]:
                        errorMessage = "Online services are being updated, service will be resumed shortly.";
                        break;
                    }
                    $("body").removeClass('success').addClass("error");
                    App.Animation.replaceText($("#new_session legend"), "Oops... not logged in.",false);
                    App.Animation.replaceText($("#new_account legend"), "Oops... account not created.",false);
                    if($("div.errorMessage").length>0){
                        $("<div>",{'class':'errorMessage', text:errorMessage});
                        $("div.errorMessage").slideDown('slow');
                    }else{
                        $("legend").after($("<div>",{'class':'errorMessage', text:errorMessage}));
                        $("div.errorMessage").slideDown('slow');
                    }
                }
            });
            
            $("form#new_session").bind('submit',
            function(e) {
                e.preventDefault();
                var email = $("#new_session :input[type=email]").val(),
                password = $("#new_session :input[type=password]").val();
                if(window.ajax_call){
                    window.ajax_call.abort();
                    App.Animation.replaceText($("#new_session :input[type=submit]"), "Sign In", false);
                    window.ajax_call = undefined;
                }else{
                    window.ajax_call = $.ajax({
                        url: Config.host + "/users/sign_in.json",
                        type: "POST",
                        dataType: "json",
                        data: "api_key=" + Config.key + "&user[email]=" + email + "&user[password]=" + password
                   
                    });
                }
                
            });
            $("form#new_account").bind('submit',
            function(e) {
                e.preventDefault();
                var email = $("#new_account :input[type=email]").val(),
                password = $("#new_account :input[type=password]#user_password").val(),
                password_confirmation = $("#new_account :input[type=password]#user_password_confirmation").val(),
                invite_code = $("#new_account :input[type=text]#user_invite_code").val();
                window.ajax_call = $.ajax({
                    url: Config.host + "/users.json",
                    type: "POST",
                    dataType: "json",
                    data: "api_key=" + Config.key + "&user[email]=" + email + "&user[password]=" + password + "&user[password_confirmation]=" + password_confirmation + "&user[invite_code]="+ invite_code 
                   
                });
            });
        }
    };
} ());