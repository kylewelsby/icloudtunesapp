/**
* @depend app.js
**/

/*jslint white: false, browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true */
/*global $, Titanium, Preferences, App */

var Config = (function() {
    return {
        key: "b4df005ad2f1c48bd1ed0b0e9b645ddb7789d225:"+Titanium.App.getGUID(),
        host: "http://icloudtunes.com",
        env: "production"
    };
}());

/*
    Environmental Configurations 
*/
if(Titanium.App.Properties.hasProperty("useBetaSite") && Titanium.App.Properties.getBool('useBetaSite') === true){
    /*
        Staging
    */
    Config.env = "staging";
    Config.host = "http://beta.icloudtunes.com";
    
}else if(Titanium.API.getEnvironment().KR_HOME.match(/\/dist\//) || Titanium.API.getEnvironment().KR_HOME.match(/\\dist\\/)){
    /*
        Titanium Development mode
    */
    Config.env = "development";
    Config.host = "http://icloudtunes.local";
}else{
    /*
        Live Producton
    */
    Config.env = "production";
    Config.host = "http://icloudtunes.com";
}

if(Config.env === "development" && (Titanium.App.Properties.hasProperty("showInspector") && Titanium.App.Properties.getBool('showInspector')) === true){
    // Titanium.UI.getCurrentWindow().showInspector();
    console.log("\n\n\n\n**** IN LOCAL DEVELOPMENT MODE ****\n\n\n\n");
}