#!/usr/bin/ruby
require 'rubygems'

begin
  root = File.expand_path(File.dirname(Titanium.App.getPath))
rescue
  root = File.dirname(__FILE__)
end
gem_path = File.join(root, '../', 'Resources', 'vendor', 'bundle','ruby','1.8')
Gem.use_paths(nil, [gem_path])

begin
  require 'json'
  require 'yaml'
  require 'mimetype_fu'
rescue LoadError
  require 'yaml'
  begin
    require File.join(File.expand_path(gem_path), 'gems', 'json-1.5.1', 'lib', 'json')
    require File.join(File.expand_path(gem_path), 'gems', 'mimetype-fu-0.1.2', 'lib', 'extensions_const')
    require File.join(File.expand_path(gem_path), 'gems', 'mimetype-fu-0.1.2', 'lib', 'mimetype_fu')
  rescue LoadError
    puts "Can't Load Gems URG"
  end
end

case RUBY_PLATFORM
when /darwin/
  OS = "darwin"
  require "osx/cocoa"
  include OSX
  OSX.require_framework 'ScriptingBridge'
  ITUNES = OSX::SBApplication.applicationWithBundleIdentifier("com.apple.itunes")
when /mingw32/
  OS = "win32"
  require 'win32ole'
  ITUNES = WIN32OLE.new('iTunes.Application')
end

def iTunes(wants="json", id=0)
  
  # if ITUNES.playerState == 1800426320 || ITUNES.playerState == 1
  # end
  case OS
  when "darwin"
    if ITUNES.isRunning?
      track = ITUNES.currentTrack
      playlist = ITUNES.sources[0].userPlaylists[0]
    end
  when "win32"
    track = ITUNES.currentTrack
    # playlist = ITUNES.Sources.ItemByName('Library').Playlists
  end
  if defined?(track.trackDatabaseID)
    dbID = track.trackDatabaseID
  elsif track.databaseID
    dbID = track.databaseID
  end

  case wants
  when "running"
    ITUNES.isRunning?
  when "json"
    # track ||= ITUNES.currentTrack
    output = { :playerState => iTunes("status"),
      :currentTime => ITUNES.playerPosition,
      :duration    => track.duration,
      :name        => track.name,
      :artist      => track.artist,
      :album       => track.album,
      :rating      => track.rating,
      :podcast     => track.podcast,
      :kind        => track.kind,
      :databaseID  => dbID
      }
    begin
      output.to_json
    rescue
      out = "{"
      output.each do |k,v|
        out << "\"#{k.to_s}\":\"#{v.to_s}\","
      end
      out << "}"
      out
    end

  when "extra"
    # track ||= ITUNES.currentTrack

    output = { :playerState   => iTunes("status"),
      :currentTime   => ITUNES.playerPosition,
      :duration      => track.duration.to_i,
      :name          => track.name,
      :artist        => track.artist,
      :album         => track.album,
      :rating        => track.rating,
      :bit_rate      => track.bitRate,
      :sample_rate   => track.sampleRate,
      :bpm           => track.bpm,
      :date_added    => track.dateAdded,
      # :disc_number => track.diskNumber,
      # :disc_count  => track.diskCount,
      :genre         => track.genre,
      :play_count    => track.playedCount,
      :rating        => track.rating,
      :release_date  => track.releaseDate,
      :track_number  => track.trackNumber,
      :year          => track.year,
      :kind          => track.kind,
      :podcast       => track.podcast,
      :databaseID    => dbID,
      # :folder      => iTunes("key"),
      :filesize      => track.size,
      :path          => iTunes("filepath"),
      :filename      => iTunes('filename'),
      :filetype      => iTunes("mimetype")
      }
    begin
      output.to_json
    rescue
      out = "{"
      output.each do |k,v|
        out << "\"#{k.to_s}\":\"#{v.to_s}\","
      end
      out << "}"
      out
    end
  when "status"
    case ITUNES.playerState
    when 1800426320
      s = "playing"
    when 1800426352
      s = "paused"
    when 1800426323
      s = "stopped"
    else
      s = ITUNES.playerState
    end
    [s, ITUNES.playerPosition]
  when "artist"
    track.artist
  when "name"
    track.name
  when "album"
    track.album
  when "artwork"
    playlist ||= ITUNES.sources[0].userPlaylists[0]
    ct = id > 0 ? id : ITUNES.currentTrack.databaseID
    ftrack ||= playlist.fileTracks.detect {|i| i.databaseID == ct}
    ftrack.artworks.first.data
  when "filename"
    begin
      case OS
      when "darwin"
        playlist ||= ITUNES.sources[0].userPlaylists[0]
        ct = id > 0 ? id : ITUNES.currentTrack.databaseID
        ftrack ||= playlist.fileTracks.detect {|i| i.databaseID == ct}
        ftrack.location.pathComponents.last
      when "win32"
        if track.location.match(/[^\\]*$/)
          track.location.match(/[^\\]*$/)[0]
        end
      end
    rescue
      return nil
    end
  when "filepath"
    begin
      case OS
      when "darwin"
        playlist ||= ITUNES.sources[0].userPlaylists[0]
        ct = id > 0 ? id : track.databaseID
        playlist.fileTracks.detect {|i| i.databaseID == ct}.location.path
      when "win32"
        track.location
      end
    rescue
      return nil
    end
  when "mimetype"
    begin
      File.mime_type?(iTunes("filepath").to_s)
    rescue
      # "audio/mpeg"
      "application/octet-stream"
    end
  else
    track
  end
rescue => e
  return e
end

puts iTunes("running")