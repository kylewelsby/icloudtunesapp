/**
* @depend app.js
**/
/*jslint white: false, browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true */
/*global $, Titanium, Tracks, App, S3, screen, window */
var Player = (function() {
    return {
        init: function() {
            Player.track = {};
            Player.status = {};
            Player.update();
            Player.failedAttempts = 0;
        },
        update: function() {
            if (window.playerStatus()) {
                Player.status = JSON.parse(window.playerStatus());
                if (Player.status.status === "playing" &&    (Player.status.database_id !== undefined || Player.status.database_id !== 0) && Player.status.database_id !== Player.track.database_id) {
                    if(!Player.failedAttempts || Player.failedAttempts < 5){
                        try {
                            console.log("Track changed, updating data.");
                            Player.track = JSON.parse(window.trackInfo());
                            if(window.Push){
                                window.Push.UI.update();
                            }
                        } catch(e) {
                            Player.failedAttempts = Player.failedAttempts + 1;
                            console.error("Failed to update track data.");
                            console.error(e);
                        }
                    }else{
                        console.error("Fatal Error: Failed gather track information " + Player.failedAttempts + " times. Restarting Pusher");
                        // Titanium.UI.createWindow('app://push.html').open();
                        App.UI.showPushWindow();
                        Titanium.UI.getCurrentWindow().close();
                        // Titanium.App.restart();
                    }
                }
            }
        }
    };
} ()),
Push = (function() {
    return {
        init: function() {
            Titanium.API.set("Push", Push);
            App.initDatabase();
            App.initFileCache();
            // Titanium.API.get("App").UI.trayItem.forcePush.enable();
            Titanium.API.get("App").UI.reset.forcePush();
            Push.lock = false;
            Push.track = {};
            Push.loaded = {};
            Push.screenWidth = 0;

            Push.UI.position();
            Player.init();
            Push.UI.update();
            setInterval(function() {
                Push.UI.position();
                Player.update();
            },
            2500);
            setInterval(function() {
                Push.waiting();
                Push.UI.status();
            },
            500);

        },
        onQuit: function() {
            if (window.CloudXHR) {
                window.CloudXHR.abort();
                App.Store.Online.set('failed', Push.loaded[0].track.id,
                function() {
                    Titanium.UI.getCurrentWindow().close();
                });
            }
        },
        waiting: function() {

            // Push Now Button Actions
            if (Titanium.API.get("pushNow") === true || (App.connected && ((Player.status.position / Player.track.duration) * 100 > Titanium.App.Properties.getInt('pushAt')))) {

                Titanium.API.set("pushNow", false);
                Push.startWorker(Player.track);
            }
        },
        check: function(track) {
            var list = App.Store.get('tracks');

            if (list === null || (list.length > 0 && $.isArray(list))) {
                if (list === null) {
                    return true;
                } else if ($.isArray(list) && !Push.inTracks(list, track.databaseID)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        },

        inTracks: function(pushed, check) {
            var c = 0;

            $.each(pushed,
            function() {
                if (parseInt(this, 10) === parseInt(check, 10)) {
                    c = c + 1;
                }
            });
            if (c > 0) {
                return true;
            } else {
                return false;
            }
        },
        startWorker: function(track) {
            var trackExtra,
            trackFile,
            tracks = [];

            if (!Push.lock && Titanium.Network.online && !Player.track.podcast) {
                
                if (Player.track.location) {
                    trackFile = Titanium.Filesystem.getFile(Player.track.location);
                    if (trackFile.isFile() && ((App.Store.get('pushed') && !Push.inTracks(App.Store.get('pushed'), Player.track.database_id)) || App.Store.get('pushed') === null)) {
                        Push.lock = true;
                        console.log("Starting Pusher Worker");
                    
                        Titanium.API.get("App").UI.trayItem.forcePush.label = "Abort push";
                        Titanium.API.get("App").UI.trayItem.forcePush.addEventListener('click',
                        function(e) {
                            Titanium.API.get("Push").abortWorker(Player.track);
                        });
                        Titanium.API.get("App").UI.trayItem.forcePush.enable();

                        App.Store.Online.set('tracks', Player.track);

                        if (App.Store.get('tracks')) {
                            tracks = App.Store.get('tracks');
                        }
                        tracks.push(Player.track);
                        App.Store.set('tracks', tracks);
                    } else {
                        if (Push.lock !== true) {
                            App.Store.Session.set('pushToAccount', 'pushed');
                        }
                    }
                } else {
                    console.log("Failed to push track. Retrying");
                    // App.Store.Session.set('pushToAccount', "error");
                }
            }
        },
        abortWorker: function(track) {
            if (Titanium.API.get("CloudXHR")) {
                Titanium.API.get("CloudXHR").abort();
                App.Store.Online.set("failed", Push.loaded[0].track.id);
            }
            Titanium.API.get("App").UI.reset.forcePush();
        },
        UI: {
            show: function() {
                $("#currentTrack:not(:visible)").slideDown('fast',
                function() {
                    // Titanium.UI.getCurrentWindow().show();
                    });
            },
            hide: function() {
                $("#currentTrack:visible").slideUp('slow',
                function() {
                    // Titanium.UI.getCurrentWindow().hide();
                    });
                // alert("hidden");
            },
            position: function() {
                if (Push.screenWidth !== screen.width) {
                    Titanium.UI.currentWindow.setX(screen.width - (Titanium.UI.currentWindow.getBounds().width));
                }
            },
            status: function() {
                $('.pushState').removeClass('success pushing error');
                switch (App.Store.Session.get('pushToAccount')) {
                case "" || null:
                    if (Titanium.Network.online) {
                        $('.pushState').text("Listening to");
                        Titanium.App.Properties.setBool('locked', false);

                    } else {
                        $('.pushState').html("<em>Offline</em> Listening to");
                    }

                    break;
                case "pushed":
                    $('.pushState').addClass('success');
                    $("#playerState").html("&#x2714;").fadeIn();
                    Titanium.App.Properties.setBool('locked', true);
                    break;
                case "pushing":
                    $("#playerState").html("&#x25B2;").fadeIn();
                    $('.pushState').addClass('pushing').text("Pushing");
                    Titanium.App.Properties.setBool('locked', true);
                    break;
                case "complete":
                    $('.pushState').addClass('success').text("Now available");
                    $("#playerState").html("&#x2714;").fadeIn();
                    Titanium.API.get("App").UI.reset.forcePush();
                    Titanium.App.Properties.setBool('locked', true);
                    break;
                case "error":
                    $('.pushState').addClass('error').text("Error occured");
                    Titanium.API.get("App").UI.reset.forcePush();
                    Titanium.App.Properties.setBool('locked', false);
                    break;
                case "failed":
                    $('.pushState').addClass('error').text("Failed to Complete");
                    Titanium.API.get("App").UI.reset.forcePush();
                    Titanium.App.Properties.setBool('locked', false);
                    break;
                case "not_running":
                    $('.pushState').text("iTunes not Open");
                    Titanium.API.get("App").UI.reset.forcePush();
                    Titanium.App.Properties.setBool('locked', false);
                    break;
                }
            },
            update: function() {

                if (Player.status.status === "playing") {
                    if ($("#currentTrack").data('track') === undefined || Player.track.database_id !== $("#currentTrack").data('track').database_id) {
                        if (Push.lock) {
                            if ($(".nowPlayingTrack").length === 0) {
                                $(".scrollText").fadeOut('slow',
                                function() {
                                    $(this).wrapInner($("<span>", {
                                        'class': 'previousTrack'
                                    }));


                                    $(".previousTrack").after($("<span>", {
                                        'class': 'nowPlayingTrack'
                                    }));
                                    var st = $("<span>", {
                                        'class': 'state',
                                        text: 'Listening To'
                                    }),
                                    na = $("<span>", {
                                        'class': 'name',
                                        text: Player.track.name
                                    }),
                                    ar = $("<span>", {
                                        'class': 'artist',
                                        text: Player.track.artist
                                    }),
                                    al = $("<span>", {
                                        'class': 'album',
                                        text: Player.track.album
                                    });
                                    $(".nowPlayingTrack").empty().append(st).append(na).append(ar).append(al);
                                    $(this).fadeIn('slow');
                                });
                            }

                        } else {

                            App.Store.Session.clear('pushToAccount');
                            $("#currentTrack .scrollText").fadeOut('slow',
                            function() {
                                $('#playerState').html("&#x25BA;").fadeIn('slow');
                                $(".nowPlayingTrack").remove();
                                if ($(".previousTrack").length) {
                                    $('.pushState').unwrap();
                                }
                                $(".name, .artist, .album", "#currentTrack").html("").hide();
                                $("#currentTrack").data('track', Player.track);
                                $("#currentTrack .name").html(Player.track.name);
                                $("#currentTrack .artist").html(Player.track.artist);
                                $("#currentTrack .album").html(Player.track.album);
                                $(".name:not(empty), .artist:not(empty), .album:not(empty)").show();
                                $(this).fadeIn('slow');
                            });
                        }
                        // Push.UI.show();
                    }

                    if ($("#currentTrack:not(.playing)").length) {
                        $('#playerState')
                        .html("&#x25BA;")
                        .fadeIn('slow');
                        $(":not(.paused) .scroll")
                        .animate({
                            opacity: '1'
                        },
                        1000, 'swing');
                        $("#currentTrack").removeClass("paused").addClass("playing");
                    }
                } else {
                    $("#currentTrack").data('track', undefined);

                    $('#playerState')
                    .html("&#x2590;&#x2590;")
                    .fadeIn('slow');

                    $(":not(.playing) .scroll")
                    .animate({
                        opacity: '.5'
                    },
                    1000, 'swing');
                    $("#currentTrack").removeClass("playing").addClass('paused');

                }
                // setTimeout(Push.UI.hide(),2000);
                /*
                if (App.Auth.signedIn()) {
                    if (window.anyPlayer().player().status() === "playing") {
                        // var track = JSON.parse(window.iTunes('json'));
                        Push.update(window.anyPlayer().info);
                    } else {
                        App.Store.Session.set('pushToAccount', 'not_running');
                    }
                } else {
                    Titanium.UI.getCurrentWindow().close();
                    clearTimeout(window.updater);
                }
                */
            }
        }
    };
} ());
