/**
**/
/*jslint browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true, white: false*/
/*global $, App, Config, Titanium */
/* vim: set ft=javascript: */

var Tracks = (function() {
    return {
        init: function(){
            Titanium.include('player.rb');
            // initiate table
            App.database.execute("CREATE TABLE IF NOT EXISTS tracks (id INTEGER PRIMARY KEY AUTOINCREMENT, database_id INTEGER NOT NULL, name TEXT NOT NULL, artist TEXT, album TEXT, location TEXT NOT NULL, status TEXT DEFAULT 'pending')");
        },
        to_hash: function(results){
            var tracks = [], track;
            while(results.isValidRow()){
                track = {
                    'id': results.field(0),
                    'name': results.field(1),
                    'artist': results.field(2),
                    'album': results.field(3),
                    'database_id': results.field(4),
                    'location': results.field(5),
                    'status': results.field(6)
                };

                tracks.push(track);
                results.next();
            }
            return tracks;
        },
        sync: function(){
            
        },
        create: function(track){
            App.database.execute("INSERT INTO tracks (name, artist, album, database_id, location) VALUES (?,?,?,?,?)", track.name, track.artist, track.album, track.database_id, track.location);
            // cache artwork.
            // get from tmp locaiton and copy to cache
            
            
            return Tracks.find_by_database_id(track.database_id);
        },
        all: function(){
            var results = App.database.execute("SELECT * FROM tracks");
            return Tracks.to_hash(results);
        },
        find_by_database_id: function(id){
            var results = App.database.execute("SELECT * FROM tracks WHERE tracks.database_id = ?", id);
            return Tracks.to_hash(results);
        },
        find_or_create_by_track: function(track){
            var find = Tracks.find_by_database_id(track.database_id);
            if(find.length === 0){
                return Tracks.create(track);
            }else{
                return find;
            }
        }
    };
} ());