/**
* @depend app.js
**/
/*jslint white: false, browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true */
/*global $, window, Titanium, App */

var Preferences = (function() {
    return {
        init: function() {
            Preferences.defaults();
            Preferences.Online.get();
            Titanium.API.addEventListener(Titanium.CLOSE,
            function() {
                Preferences.Online.set();
            });
            Titanium.API.addEventListener(Titanium.EXIT,
            function() {
                Preferences.Online.set();
            });

            Preferences.UI.setValues();

            $(":input").change(function() {
                if (window.update_preferences) {
                    clearTimeout(window.update_preferences);
                }
                window.update_preferences = setTimeout(function() {
                    Preferences.Online.set();
                },
                2000);
            });
        },
        Online: {
            set: function() {
                console.debug("Syncing Preferences with WebApp");
                var props = "{",
                keys = Titanium.App.Properties.listProperties();
                $.each(keys,
                function(i, key) {
                    props = props + (i !== 0 ? ",": "") + '"' + key + '":"' + Titanium.App.Properties.getString(key) + '"';
                });
                props = props + "}";

                App.Store.Online.set("preferences", JSON.parse(props));
            },
            get: function() {
                App.Store.Online.get("preferences");
            },
            results: function(data) {
                var prop = Titanium.App.Properties;
                console.debug(data);
                prop.setBool("openPushOnStartup", data.user.open_push_on_startup);
                prop.setInt("pushAt", data.user.push_at);
                prop.setBool("betaUser", data.user.beta);
                Preferences.UI.setValues();
            }
        },
        defaults: function() {
            var prop = Titanium.App.Properties;
            if (!prop.hasProperty("userAuthToken")) {
                prop.setString('userAuthToken', '');
            }
            if (!prop.hasProperty("openPushOnStartup")) {
                prop.setBool("openPushOnStartup", true);
            }
            if (!prop.hasProperty('useBetaSite')) {
                prop.setBool('useBetaSite', false);
            }
            if (!prop.hasProperty('showInspector')) {
                prop.setBool('showInspector', false);
            }

            if (!prop.hasProperty('locked')) {
                prop.setBool('locked', false);
            }

            if (!prop.hasProperty('pushNow')) {
                prop.setBool('pushNow', false);
            }
            if (!prop.hasProperty('betaUser')) {
                prop.setBool('betaUser', false);
            }
            if (!prop.hasProperty('pushAt')) {
                prop.setInt('pushAt', 50);
            }
            if (!prop.hasProperty('pushOnlySkips')) {
                prop.setInt('pushOnlySkips', 10);
            }
            if (!prop.hasProperty('pushOnlyPlays')) {
                prop.setInt('pushOnlyPlays', 1);
            }
        },
        getString: function(key) {
            if (Titanium.App.Properties.hasProperty(key)) {
                return Titanium.App.Properties.getString(key);
            }
            return false;
        },
        getInt: function(key) {
            if (Titanium.App.Properties.hasProperty(key)) {
                return Titanium.App.Properties.getInit(key);
            }
            return false;
        },
        getBool: function(key) {
            if (Titanium.App.Properties.hasProperty(key)) {
                return Titanium.App.Properties.getBool(key);
            }
            return false;
        },
        clear: function(key) {
            if (Titanium.App.Properties.hasProperty(key)) {
                Titanium.App.Properties.removeProperty(key);
            }
            return false;
        },
        UI: {
            setValues: function() {
                var prop = Titanium.App.Properties;

                $(":input[name=openPushOnStartup]").change(function() {
                    prop.setBool('openPushOnStartup', $(this).attr('checked'));
                })
                .attr("checked", prop.getBool("openPushOnStartup"));

                $(":input[name=pushAt]").change(function() {
                    prop.setInt('pushAt', parseInt($(this).val(), 10));
                    $("output#pushAt").text($(this).val() + "%");
                })
                .val(prop.getInt("pushAt"));
                $("output#pushAt").text(prop.getInt("pushAt") + "%");

                $(":input[name=pushOnlyPlays]").change(function() {
                    prop.setInt('pushOnlyPlays', parseInt($(this).val(), 10));
                    $("output#pushOnlyPlays").text($(this).val() + "%");
                })
                .val(prop.getInt("pushOnlyPlays"));
                $("output#pushOnlyPlays").text(prop.getInt("pushOnlyPlays"));

                $(":input[name=pushOnlySkips]").change(function() {
                    prop.setInt('pushOnlySkips', parseInt($(this).val(), 10));
                    $("output#pushOnlySkips").text($(this).val() + "%");
                })
                .val(prop.getInt("pushOnlySkips"));
                $("output#pushOnlySkips").text(prop.getInt("pushOnlySkips"));

                if (prop.getBool("betaUser") === true) {
                    $("section#advancedSettings").show();
                }

                $(":input[name=useBetaSite]").change(function() {
                    prop.setBool('useBetaSite', $(this).attr('checked'));
                })
                .attr("checked", prop.getBool("useBetaSite"));

                $(":input[name=showInspector]").change(function() {
                    prop.setBool('showInspector', $(this).attr('checked'));
                })
                .attr("checked", prop.getBool("showInspector"));
            },
            onSync: function(status) {
                var message;
                if (status === "success") {
                    message = "Successfully synced preferences.";
                } else {
                    message = "Failed to sync preferences.";
                }
                $("body").append($("<div>", {
                    'class': 'notice success',
                    text: message
                }));
                $(".notice").fadeIn('fast',
                function() {
                    $(this).delay(2000).fadeOut('fast').remove();
                });
            }
        }
    };
} ());