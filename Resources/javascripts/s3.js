/**
* @depend app.js
**/
/*jslint white: false, browser: true, devel: true, onevar: true, undef: true, nomen: true, eqeqeq: true, bitwise: true, regexp: true, newcap: true, immed: true */
/*global $, Titanium, App, Push, window, unescape: true, btoa: true */

var S3 = (function() {
    return {
        Storage: {
            put: function(cloud_json, filetype, postData) {
                console.info("Pushing to Cloud.");
                var xhr = Titanium.Network.createHTTPClient(),
                auth = cloud_json.auth,
                date = cloud_json.date,
                url = cloud_json.url;
                xhr.onsendstream = function(e){
                    console.log(e.progress);
                };
                xhr.onerror = function(){
                    console.error("Error pushing to Cloud. ");
                    console.debug(url);
                    console.debug(date);
                    App.Store.Session.set('pushToAccount', "error");
                };
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        console.debug("Completed pushing to Cloud.");
                        App.Store.Online.set('complete', Push.loaded.library.track.id);
                    }
                    if (xhr.readyState === 4 && (xhr.status !== 200 && xhr.status !== 304)) {
                        App.Store.Session.set('pushToAccount','error');
                        console.error("Failed pushing to Cloud. ");
                        App.Store.Online.set('failed', Push.loaded.library.track.id);
                        return;
                    }
                };
                // xhr.setTimeout(5*60*1000);
                xhr.open("PUT", url, true);
                if (postData) {
                    xhr.setRequestHeader('Date', date);
                    xhr.setRequestHeader('Content-Type', filetype);
                    xhr.setRequestHeader('Authorization', auth);
                    Titanium.API.set("CloudXHR", xhr);
                    xhr.send(postData);
                }else{
                    console.error("No File to Upload");
                }
            },
            get: function(filename, filetype, folder){
                // var dir = encodeURIComponent(folder + filename),
                // xhr = new XMLHttpRequest(),
                // date = $.format.date(new Date(),"E, dd MMM yyyy HH:mm:ss z"),
                // Authorization = S3.Storage.Authorization("GET", filename, filetype, 0, folder, date),
                // path = "http://" + Config.s3Host + "/" + dir;
                // 
                // xhr.open("GET", path, true);
                // xhr.setRequestHeader('Date', date);
                // xhr.setRequestHeader('Content-Type', filetype);
                // xhr.setRequestHeader('Authorization', Authorization);
                // xhr.send();
                
            }
        }
    };
} ());