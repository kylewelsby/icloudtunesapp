#!/usr/bin/env python

import json
try:
  import win32com.client
except ImportError:
  pass
  
try:
  from Foundation import *
  from ScriptingBridge import *
except ImportError:
  pass
  
iTunes = SBApplication.applicationWithBundleIdentifier_("com.apple.iTunes")

print iTunes.currentTrack().name()