#!/usr/bin/ruby
# -*- encoding: utf-8 -*-
require "time"
require "date"

def convert_kind(kind)
  case kind
  when "MPEG audio file"
    "audio/mpeg"
  when "MPEG audio stream"
    "audio/mpeg"
  when "AAC audio file"
    "audio/m4a"
  when "AAC audio stream"
    "audio/m4a"
  when "Purchased AAC audio file"
    "audio/m4a"
  when "Protected AAC audio file"
    "audio/m4p"
  when "WAV audio file"
    "audio/wav"
  end
end

def trackInfo
  osascript = %x(osascript -ss -e 'tell app "iTunes" to return {track_number : track number, database_id : database id, track_count : track count, rating : rating, year : year, duration :duration, bit_rate : bit rate, sample_rate : sample rate, played_count : played count, skipped_count : skipped count, podcast : podcast, name : name, artist : artist, album : album, kind : kind, date_added : date added, disc_number : disc number, genre : genre, played_date : played date, skipped_date : skipped date} of current track').rstrip
  parseTrackInfo(osascript)
end

def parseTrackInfo(osascript)
  e = osascript.gsub('{track_number:','').gsub(/\}\z/, '').split(/,\s\w+:/).collect {|e| e.gsub('"','')}
   res = "{"
  res << "\"database_id\":#{e[1]},"
  res << "\"track_number\":#{e[0]},"
  res << "\"track_count\":#{e[2]},"
  res << "\"rating\":#{e[3]},"
  res << "\"year\":#{e[4]},"
  res << "\"duration\":#{e[5] == "missing value" ? "null" : e[5]},"
  res << "\"bit_rate\":#{e[6] == "missing value" ? "null" : e[6]},"
  res << "\"sample_rate\":#{e[7] == "missing value" ? "null" : e[7]},"
  res << "\"played_count\":#{e[8]},"
  res << "\"skipped_count\":#{e[9]},"
  res << "\"podcast\":#{e[10]},"
  res << "\"name\":\"#{e[11]}\","
  res << "\"artist\":\"#{e[12]}\","
  res << "\"album\":\"#{e[13]}\","
  res << "\"kind\":\"#{e[14]}\","
  res << "\"mimetype\":\"#{convert_kind(e[14])}\","
  res << "\"filetype\":\"#{convert_kind(e[14])}\","
  res << "\"date_added\":\"#{Time.parse(e[15])}\","
  res << "\"disk_number\":#{e[16] == "missing value" ? "null" : e[16]},"
  res << "\"genre\":\"#{e[17]}\","
  res << "\"played_date\":\"#{Time.parse(e[18])}\","
  res << "\"skipped_date\":\"#{Time.parse(e[19])}\","
  res << "\"filename\":\"#{trackLocation.split("/").last if trackLocation}\","
  res << "\"location\":\"#{trackLocation}\","
  res << "\"artwork\":\"#{trackArtwork}\""
  res << "}"
  res.to_s
rescue 
  return nil
end

def trackName
  %x(osascript -e 'tell app "iTunes" to return name of current track').rstrip
end

def trackArtist
  %x(osascript -e 'tell app "iTunes" to return artist of current track').rstrip
end

def trackAlbum
  %x(osascript -e 'tell app "iTunes" to return album of current track').rstrip
end

def trackLocation
  %x(osascript -e 'tell app "iTunes"' -e "set trk to current track" -e 'if kind of trk is "MPEG audio stream" then' -e "return address of trk" -e "else" -e "set l to trk's location" -e "return POSIX path of l" -e "end if" -e "end tell").rstrip
end

def trackArtwork
  dbid = %x(osascript -e 'tell app "iTunes" to return database id of current track').rstrip

  location = "/tmp/iTunesArtwork.pict"
  %x(touch #{location})
  %x(osascript -e 'tell app "iTunes"' -e 'set aTemp to POSIX file "#{location}" as alias' -e 'set aTrackArtwork to null' -e "set response to null" -e "if (count of artwork of current track) > 0 then" -e "set aTrackArtwork to data of artwork 1 of current track" -e "set fileRef to (open for access aTemp with write permission)"  -e "try" -e "set eof fileRef to 512" -e "write aTrackArtwork to fileRef starting at 513" -e "close access fileRef" -e 'set response to "/tmp/iTunesArtwork.pict"' -e "on error" -e "try" -e "close access fileRef" -e "set response to null" -e "end try" -e "end try" -e "end if" -e "return response" -e "end tell").rstrip
end

def playerStatus
  osascript = %x(osascript -e 'tell app "iTunes" to return {player state, player position}').rstrip
  e = osascript.split(",")
  if e[0] === "playing"
    sta = e[0]
    dbid = %x(osascript -e 'tell app "iTunes" to return database id of current track').rstrip
  else
    sta = "stopped"
    dbid = 0
  end
  if e[1]
    pos = e[1]
  else
    pos = 0
  end
  res = "{"
  res << "\"status\":\"#{sta}\","
  res << "\"position\":#{pos},"
  res << "\"database_id\":#{dbid}"
  res << "}"
rescue
  return nil
end

def makePlaylist(name)
  %x(osascript -e 'tell app "iTunes"' -e 'if (playlist (\"#{name}\") exists) is false then' -e'make new user playlist with properties {name: \"#{name}\"}' -e 'end if' -e 'end tell').rstrip
end

# puts "playing,".split(",")[0]

puts trackInfo